import Rectangle_Java.Rectangle;

public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
    
        Rectangle rectangle1 = new Rectangle();
        Rectangle rectangle2 = new Rectangle(3.2f, 2.1f);

        System.out.println("Rectangle_1:");
        System.out.println(rectangle1);
        System.out.println("Area:" + rectangle1.getArea());
        System.out.println("Perimeter:" + rectangle1.getPerimeter());
        System.out.println(rectangle1.toString());

        System.out.println("Rectangle_2:");
        System.out.println(rectangle2);
        System.out.println("Area:" + rectangle2.getArea());
        System.out.println("Perimeter:" + rectangle2.getPerimeter());
        System.out.println(rectangle2.toString());
    
    }
}
